//
//  ViewController.swift
//  Localization
//
//  Created by Sierra 4 on 01/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//
import Localize_Swift
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblone: UILabel!
    @IBOutlet weak var lblTwo: UILabel!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var sgmLanguage: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
//        lblone.text = NSLocalizedString("Labelone".localized(), comment: "HI")
//        lblTwo.text = NSLocalizedString("LabelTwo".localized(), comment: "HI")
//        txtField.placeholder = NSLocalizedString("field".localized(), comment: "HI")
    }
    @IBAction func sgmSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            Localize.setCurrentLanguage("en")
            lblone.textAlignment = .left
            lblTwo.textAlignment = .left
            txtField.textAlignment = .left
            rel()
//            reload()
        }
        else{
            Localize.setCurrentLanguage("ar")
            lblone.textAlignment = .right
            lblTwo.textAlignment = .right
            txtField.textAlignment = .right
            rel()
//           reload()
        }
    } 
//    func reload(){
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ViewController")
//        UIApplication.shared.keyWindow?.rootViewController = viewController
//    }
    func rel(){
        lblone.text = NSLocalizedString("Labelone".localized(), comment: "HI")
        lblTwo.text = NSLocalizedString("LabelTwo".localized(), comment: "HI")
        txtField.placeholder = NSLocalizedString("field".localized(), comment: "HI")
    }
}
